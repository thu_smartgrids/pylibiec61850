# General description
This project contains basic information on the open-source libiec61850 software
 stack, mainly regarding the compliling of pylibiec61850 into Python compatible packages. Pylibiec61850 is frequently used by the Smart Grids Research Group of the Ulm University of Applied Sciences
 (DE: Technische Hochschule Ulm) for different Smart Grids applications, more details on this are coming later. 

- Short introduction (see the text below)
- [Instruction: Compiling for Python in windows environment](./Compiling_Python_windows.md)
- [Instruction: Compiling for Python in Linux (AMD x86-64) environment](./Compiling_Python_linux_x86-64.md)
- [Instruction: Compiling for Python in Linux (ARM64) environment (raspberry pi 4/5)](./Compiling_Python_linux_arm64.md)
- [Lib: Latest windows pyiec61850 lib](./libiec61850_compiled_Windows)
- [Lib: Latest linux AMD64 pyiec61850 lib](./libiec61850_compiled_Linux_x86-64)
- [Lib: Latest linux ARM64 pyiec61850 lib](./libiec61850_compiled_Linux_arm64)
- [Container: Docker project for linux compiling and testing](./libiec61850_compiled_Linux_x86-64/libiec61850_linux_compiler_tester)


### "The" library
**_Taken from the libiec61850 homepage:_**

This library provides an implementation of IEC61850 on top of the MMS (Manufacturing Message Specification) protocol in standard C. It also provides support for intra-substation communication via GOOSE. The goal of this project is to provide an implementation that is very portable and can run on embedded systems and micro-controllers. Also provided is a set of simple examples that can be used as a starting point for own applications. The library also contains a .NET wrapper to allow the library to be used easily in high-level languages like C#.
This implementation runs on embedded systems, embedded Linux systems as well as on desktop computers running Linux, Windows or MacOS.

**_In our words:_**

The libiec61850 library is a magic tool for the realisation of smart grid applications,
especially with its full protocol stack for MMS and the API for model creation.

> Note: as the substation-automation is not our research focus, we don't have much experience on the application and testing of the GOOSE/SV stack, therefore no WARRANTY for the compiled pylibiec61850 regarding GOOSE/SV.

### libiec61850 for Python
As Python is more or less the standard programming language in our research group, we urgently needed 
a library that fits into Python-based framework.

Although the lib was initially developed in C environment, the former colleague J. Morris did find
a way to compile it into Python compatible library for further use in windows environment. 
Based on that, the SGFG could successfully demonstrate many brilliant IEC 61850 server and client
applications, these include:

Later S.Chen took over his documentation and managed to compile the libIEC61850 stack
into Python suite that also works in Linux environment, which will be one of the elementary blueprints 
of his PhD-thesis.


### Official websites:
- Homepage libiec61850: https://libiec61850.com/
- Github libiec61850: https://github.com/mz-automation/libiec61850
- libiec61850 API reference model: https://support.mz-automation.de/doc/libiec61850/c/latest/index.html
- IEC61850 testing tool IED Explorer: https://github.com/ttgzs/iedexplorer; https://sourceforge.net/projects/iedexplorer/
- Docker destkop: https://www.docker.com

# License
These parts come with their own copyright and licence:
- libiec61850 (incl. pylibiec61850) has Copyright (c) Michael Zillgith, MZ Automation GmbH
- test tool IEDExplorer has Copyright (C) 2013 Pavel Charvat
- Docker Desktop is licensed as part of a free (Personal) or paid Docker subscription (Pro, Team or Business).

Copyright of the test script:
- Copyright (c) 2019-2024 J. Morris, S. Chen, Ulm University of Applied Sciences [THU](http://www.thu.de); Smart Grids Research Group [SGFG](https://studium.hs-ulm.de/de/org/iea/smartgrids/)

The compiled python libs and associated test scripts are provided under the license of GNU GPL v3. We hope they could be useful, but WITHOUT ANY WARRANTY. See the GNU General Public License for more details.

The Smart Grids Research Group keeps seeking solutions for the current and future smart grids, we are open to further discussion.
